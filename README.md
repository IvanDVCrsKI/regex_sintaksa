# regex_sintaksa

## Sintaksa


```. ```Match any character except newline

```\w ```Match any alphanumeric character

```\s``` Match any whitespace character

```\d ```Match any digit

```\b``` Match the beginning or end of a word

```^ ```Match the beginning of the string

```$ ```Match the end of the string 

```* ```Repeat any number of times

```+ ```Repeat one or more times

```? ```Repeat zero or one time

```\n ```Match a line feed (or newline).

```{n} ```Repeat n times

```{n,m} ```Repeat at least n, but no more than m times

```{n,} ```Repeat at least n times 

```[ ] ```Match a range of characters contained within the square brackets

```[^ ] ```Match a character which is not one of those contained within the square brackets.

```\< ```Matches the beginning of a word.

```\> ```Matches the end of a word.

```( )```Group part of the regular expression.

```(?<=x) ```Positive lookbehind. x is some expresion eg.[A-Z]

```(?<!x) ```Negative lookbehind.

```(?=x) ```Positive lookahead.

```(?!x)``` Negative lookahead.

```|``` Match what is on either the left or right of the pipe symbol.

## Primeri za regex 

escaping special chars 
```\^ \. \\```

example 
```$ man dpkg | grep “\-S”```

example:
```^\s\s[A-Z] ```

Match any line that starts “^” with 2 whitespace characters “\s\s” and after that contains any uppercase character “[A-Z]”

```grep -oP '(?<=<div class="Message"> ).*?(?= </div>)' file```

```grep -c``` display total number of lines that match 

```grep -n ```line number in file where pattern was found 


```man grep | grep -n "-n"``` za vise lolz

```'^\s\s[A-Z].*[<]'```

Match any line that starts “^” with 2 whitespace characters “\s\s” and after that contains any uppercase character “[A-Z]” and stop when you find < character “.*[<]”  

```grep '^\s\s[A-Z].*[r,t]$'```

Match any line that starts “^” with 2 whitespace characters “\s\s” and after that contains any uppercase character “[A-Z]” and stop when you find r,t at the end of the line  

```grep '^\s\s*.*[r,t,p,a,e,) ]$' ```
